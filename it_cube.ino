int led[] = {3, 5, 6, 9, A1, A2, A3, A4};
int LDR = A0;
int ldrvalue = 0;
int GREEN = 11;
int YELLOW = 12;
int RED = 13;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(A4, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(LDR, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  ldrvalue = analogRead(LDR);
  Serial.println(ldrvalue);
  if (ldrvalue < 720) //left
  {
    digitalWrite(YELLOW, HIGH);
    //PATTERN 1
    digitalWrite(3, 0);
    digitalWrite(5, 0);
    digitalWrite(6, 0);
    digitalWrite(9, 0);
    digitalWrite(A4, HIGH);
    delay(200);
    digitalWrite(3, 0);
    digitalWrite(5, 0);
    digitalWrite(9, 0);
    digitalWrite(A4, 0);
    digitalWrite(A3, HIGH);
    delay(200);
    digitalWrite(3, 0);
    digitalWrite(9, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    digitalWrite(A2, HIGH);
    delay(200);
    digitalWrite(9, 0);
    digitalWrite(A2, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    digitalWrite(A1, HIGH);
    delay(200);

    digitalWrite(A1, 0); //All led will 0
    digitalWrite(A2, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    digitalWrite(3, 0);
    digitalWrite(5, 0);
    digitalWrite(6, 0);
    digitalWrite(9, 0);
    delay(200);

    //pause

    digitalWrite(A1, HIGH);
    digitalWrite(A2, HIGH);
    digitalWrite(A3, HIGH);
    digitalWrite(A4, HIGH);
    delay(200);
    digitalWrite(A1, 0);
    digitalWrite(A2, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    delay(200);
  }
  else {
    digitalWrite(YELLOW, LOW);
  }

  if (ldrvalue > 950) //middle
  {
    digitalWrite(GREEN, HIGH);
    //PATTERN 2
    digitalWrite(A1, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(3, HIGH);
    digitalWrite(5, 0);
    delay(200);
    digitalWrite(5, HIGH);
    digitalWrite(6, 0);
    delay(200);
    digitalWrite(6, HIGH);
    digitalWrite(9, 0);
    delay(200);
    digitalWrite(A1, 0);
    digitalWrite(A2, HIGH);
    delay(200);
    digitalWrite(A2, 0);
    digitalWrite(A3, HIGH);
    delay(200);
    digitalWrite(A3, 0);
    digitalWrite(A4, HIGH);
    delay(200);
    digitalWrite(9, HIGH);
    digitalWrite(6, 0);
    delay(200);
    digitalWrite(6, HIGH);
    digitalWrite(5, 0);
    delay(200);
    digitalWrite(5, HIGH);
    digitalWrite(3, 0);
    delay(200);
    digitalWrite(A4, 0);
    digitalWrite(A3, HIGH);
    delay(200);
    digitalWrite(A3, 0);
    digitalWrite(A2, HIGH);
    delay(200);
    digitalWrite(A2, HIGH);
    digitalWrite(3, 0);
    digitalWrite(5, 0); //6th led
    digitalWrite(3, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(6, 0); //7th led
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(A3, HIGH);
    digitalWrite(A2, 0);
    digitalWrite(6, 0); //11th led
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(A3, HIGH);
    digitalWrite(A2, 0);
    digitalWrite(5, 0); //10th led
    digitalWrite(3, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);

    digitalWrite(A1, 0); //All led will 0
    digitalWrite(A2, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    digitalWrite(3, 0);
    digitalWrite(5, 0);
    digitalWrite(6, 0);
    digitalWrite(9, 0);
    delay(200);

    //pause

    digitalWrite(A1, HIGH);
    digitalWrite(A2, HIGH);
    digitalWrite(A3, HIGH);
    digitalWrite(A4, HIGH);
    delay(200);
    digitalWrite(A1, 0);
    digitalWrite(A2, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    delay(200);
  }
  else {
    digitalWrite(GREEN, LOW);
  }

  if ((ldrvalue > 720) && (ldrvalue < 950)) //right
  {
    digitalWrite(RED, HIGH);
    //PATTERN 3
    digitalWrite(A1, HIGH);
    digitalWrite(A2, HIGH);
    digitalWrite(A3, HIGH);
    digitalWrite(A4, HIGH);
    digitalWrite(3, 0);
    delay(200);
    digitalWrite(A1, HIGH);
    digitalWrite(A2, HIGH);
    digitalWrite(A3, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(5, 0);
    delay(200);
    digitalWrite(A1, HIGH);
    digitalWrite(A2, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, 0);
    delay(200);
    digitalWrite(A1, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, 0);
    delay(200);

    digitalWrite(A1, 0); //All led will 0
    digitalWrite(A2, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    digitalWrite(3, 0);
    digitalWrite(5, 0);
    digitalWrite(6, 0);
    digitalWrite(9, 0);
    delay(200);

    //pause

    digitalWrite(A1, HIGH);
    digitalWrite(A2, HIGH);
    digitalWrite(A3, HIGH);
    digitalWrite(A4, HIGH);
    delay(200);
    digitalWrite(A1, 0);
    digitalWrite(A2, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    delay(200);

    //PATTERN 4
    digitalWrite(A4, HIGH); //16th led
    digitalWrite(9, 0);
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    delay(200);
    digitalWrite(6, 0); //15th led
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(5, 0); //14th led
    digitalWrite(3, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(3, 0); //13th led
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(A4, 0); //12th led
    digitalWrite(A3, HIGH);
    digitalWrite(9, 0);
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    delay(200);
    digitalWrite(6, 0); //11th led
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(5, 0); //10th led
    digitalWrite(3, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(3, 0); //9th led
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(A3, 0); //8th led
    digitalWrite(A2, HIGH);
    digitalWrite(9, 0);
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    delay(200);
    digitalWrite(6, 0); //7th led
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(5, 0); //6th led
    digitalWrite(3, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(3, 0); //5th led
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(A2, 0); //4th led
    digitalWrite(A1, HIGH);
    digitalWrite(9, 0);
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    delay(200);
    digitalWrite(6, 0); //3th led
    digitalWrite(3, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(5, 0); //2th led
    digitalWrite(3, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);
    digitalWrite(3, 0); //1st led
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(9, HIGH);
    delay(200);

    digitalWrite(A1, 0); //All led will 0
    digitalWrite(A2, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    digitalWrite(3, 0);
    digitalWrite(5, 0);
    digitalWrite(6, 0);
    digitalWrite(9, 0);
    delay(200);

    //pause

    digitalWrite(A1, HIGH);
    digitalWrite(A2, HIGH);
    digitalWrite(A3, HIGH);
    digitalWrite(A4, HIGH);
    delay(200);
    digitalWrite(A1, 0);
    digitalWrite(A2, 0);
    digitalWrite(A3, 0);
    digitalWrite(A4, 0);
    delay(200);
  }
  else {
    digitalWrite(RED, LOW);
  }
}

